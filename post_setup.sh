#!/usr/bin/env sh
set -e

systemctl --user enable --now floating-terminal.service
~/.config/sway/update-config.sh
