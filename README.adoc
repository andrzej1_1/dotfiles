= dotfiles

This repository contains my system configuration.

== Automated setup

From non-root user run following script:

[source,shell]
----
$ ./setup.sh
----

Then reboot machine and execute:

[source,shell]
----
$ ./post_setup.sh
----

== Development notes

Setup script MUST follow these requirements:

 - be idempotent - useful for testing
 - be non-destructive (might override some files, but explicit delete)
 - be non-interactive, except `sudo` calls

=== Keyboard

Mappings are layout agnostic, altough they were intially created on US one. With that said you can expect that 'hjkl' movement on Dvorak will be done using 'htns'.

=== Binding keys

Do not use `bindsym` for binding keyboard, because it is not portable
accross different layouts, e.g. when you use Dvorak. `bindcode` should be used instead.
