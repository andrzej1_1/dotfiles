#!/usr/bin/env bash

# DISPLAY is dirty workaround for Sway integration...
DISPLAY=:0 alacritty --title="Floating terminal - System"
