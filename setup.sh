#!/usr/bin/env sh
set -e

pushd . > /dev/null
SCRIPT_PATH="${BASH_SOURCE[0]}"
if ([ -h "${SCRIPT_PATH}" ]); then
  while([ -h "${SCRIPT_PATH}" ]); do cd `dirname "$SCRIPT_PATH"`;
  SCRIPT_PATH=`readlink "${SCRIPT_PATH}"`; done
fi
cd `dirname ${SCRIPT_PATH}` > /dev/null
SCRIPT_PATH=`pwd`;
popd  > /dev/null

PrintYellow() {
  YELLOW='\033[0;33m'
  NC='\033[0m' # No Color
  printf "${YELLOW}$@${NC}\n"
}

PrintGreen() {
  GREEN='\033[0;32m'
  NC='\033[0m' # No Color
  printf "${GREEN}$@${NC}\n"
}

PacmanInstall () {
  PACKAGES=$@
  sudo pacman -S                  \
    --noconfirm  `# no prompts`   \
    --quiet      `# no output`    \
    --needed     `# no reinstall` \
    ${PACKAGES}
}

PacmanForceInstall () {
  PACKAGES=$@
  sudo pacman -S                  \
    --noconfirm  `# no prompts`   \
    --quiet      `# no output`    \
    ${PACKAGES}
}

AurInstall () {
  PACKAGE_NAME="$1"
  # skip if already installed
  IS_INSTALLED=0
  pacman -Q --info ${PACKAGE_NAME} 1>/dev/null 2>/dev/null || IS_INSTALLED=$?
  if [[ "${IS_INSTALLED}" -eq 0 ]]; then
    echo "warning: ${PACKAGE_NAME} is already installed -- skipping"
    return 0
  fi
  # proceed with installation
  TMP_DIR=$(mktemp --directory)
  curl --silent https://aur.archlinux.org/cgit/aur.git/snapshot/${PACKAGE_NAME}.tar.gz \
    | tar -xz -C "${TMP_DIR}"
  (
    cd "${TMP_DIR}" && cd "${PACKAGE_NAME}"
    makepkg \
      --syncdeps       `# download missing dependencies`   \
      --rmdeps         `# remove dependencies after build` \
      --install        `# install package after build `    \
      --needed         `# no reinstall`                    \
      --noconfirm      `# no prompts`                      \
      --noprogressbar  `# no progress bar for downloading` \
  )
  rm -rf "${TMP_DIR}"
}

PrintYellow ""
PrintYellow "-- INSTALLATION PHASE --"
PrintYellow ""

PrintGreen "Reinstalling archlinux-keyring (required for latest PGP keys)"
PacmanForceInstall archlinux-keyring

PrintGreen "Installing base-devel group - base development tools"
PacmanInstall base-devel

PrintGreen "Installing sway - tiling Wayland compositor"
PrintGreen " + xorg-xwayland - X server for Wayland"
PacmanInstall sway xorg-xwayland

PrintGreen "Installing swaybg - wallpaper for Wayland"
PacmanInstall swaybg

PrintGreen "Installing swaylock - screen locker for Wayland"
PacmanInstall swaylock

PrintGreen "Installing waybar - highly customizable Wayland bar"
PacmanInstall waybar

PrintGreen "Installing tmux - terminal multiplexer"
PacmanInstall tmux

PrintGreen "Installing bash-completion - bash command completions"
PacmanInstall bash-completion

PrintGreen "Installing reflector - script for refreshing Pacman mirror list"
PacmanInstall reflector

PrintGreen "Installing zsh - shell"
PacmanInstall zsh

PrintGreen "Installing fzf - fuzzy finder"
PacmanInstall fzf

PrintGreen "Installing common tools"
PrintGreen " * wget"
PrintGreen " * curl"
PacmanInstall wget curl

PrintGreen "Installing yay - AUR helper"
AurInstall yay

PrintGreen "Installing tbsm - terminal based session manager"
AurInstall tbsm

PrintGreen "Installing alacritty - GPU-accelerated terminal emulator"
PacmanInstall alacritty

PrintGreen "Installing neovim - hyperextensible Vim-based text editor"
PrintGreen " + vim - original editor as fallback"
PacmanInstall neovim vim

PrintGreen "Installing brave - original editor as fallback"
AurInstall brave-bin

PrintGreen "Installing wl-clipboard - clipboard utils for Wayland"
PacmanInstall wl-clipboard

PrintGreen "Installing slurp - region selector for Wayland"
PacmanInstall slurp

PrintGreen "Installing ttf-hack - amazing font for terminals and IDEs"
PacmanInstall ttf-hack

PrintGreen "Installing mako - notification daemon"
PacmanInstall mako

PrintGreen "Installing dex - startup launcher for .desktop files"
PacmanInstall dex

PrintGreen "Installing xdg-user-dirs - tool for managing user directories"
PacmanInstall xdg-user-dirs

PrintGreen "Installing nnn - terminal file browser"
PacmanInstall nnn

PrintGreen "Installing mo-git - template engine"
AurInstall mo-git

PrintGreen "Installing nerd-fonts-meta - pack of various fonts and glyphs"
AurInstall nerd-fonts-meta

PrintGreen "Installing ttf-google-fonts-git - common fonts from Google project"
AurInstall ttf-google-fonts-git

PrintGreen "Installing oh-my-zsh-git - configs for zsh"
AurInstall oh-my-zsh-git

PrintGreen "Installing lazygit - terminal UI for git"
PacmanInstall lazygit

PrintGreen "Installing git-revise - efficient rebase command"
AurInstall git-revise

PrintGreen "Installing wlsunset - gamma adjustments for Wayland"
AurInstall wlsunset

PrintGreen "Installing grim - screenshot utility for Wayland"
PacmanInstall grim

PrintGreen "Installing keepassxc - password manager"
PacmanInstall keepassxc

PrintGreen "Installing megasync-bin - tool for syncing with MEGA cloud"
AurInstall megasync-bin

PrintGreen "Installing firefox - web browser"
PacmanInstall firefox

PrintGreen "Enable Wayland support in Qt 5/6"
PacmanInstall qt5-wayland qt6-wayland

PrintYellow ""
PrintYellow "-- CONFIGURATION PHASE --"
PrintYellow ""

PrintGreen "Configuring tmux"
cp "${SCRIPT_PATH}/.tmux.conf" ~/.tmux.conf

PrintGreen "Configuring zsh"
sudo chsh -s /bin/zsh ${USER}
cp "${SCRIPT_PATH}/.zprofile" ~/
cp "${SCRIPT_PATH}/.zshrc" ~/

PrintGreen "Configuring sway"
mkdir -p ~/.config
cp -r "${SCRIPT_PATH}/.config/sway" ~/.config/
mkdir -p ~/Pictures
cp -r "${SCRIPT_PATH}/Pictures/Wallpapers" ~/Pictures/
mkdir -p ~/.scripts
cp "${SCRIPT_PATH}/.scripts/floating-terminal.sh" ~/.scripts/
mkdir -p ~/.config/systemd/user
cp "${SCRIPT_PATH}/.config/systemd/user/floating-terminal.service" ~/.config/systemd/user/
cp "${SCRIPT_PATH}/.config/systemd/user/sway-session.target" ~/.config/systemd/user/

PrintGreen "Configuring alacritty"
mkdir -p ~/.config
cp -r "${SCRIPT_PATH}/.config/alacritty" ~/.config/

PrintGreen "Configuring waybar"
mkdir -p ~/.config
cp -r "${SCRIPT_PATH}/.config/waybar" ~/.config/

PrintGreen "Configuring tbsm"
mkdir -p ~/.config
cp -r "${SCRIPT_PATH}/.config/tbsm" ~/.config/

PrintGreen "Disabling Power keys (ACPI events)"
sudo sed -ri "s/^#?(HandlePowerKey=).*/\1ignore/g" /etc/systemd/logind.conf
sudo sed -ri "s/^#?(HandleSuspendKey=).*/\1ignore/g" /etc/systemd/logind.conf
sudo sed -ri "s/^#?(HandleHibernateKey=).*/\1ignore/g" /etc/systemd/logind.conf

PrintGreen ""
PrintGreen "██████   ██████  ███    ██ ███████ ██ "
PrintGreen "██   ██ ██    ██ ████   ██ ██      ██ "
PrintGreen "██   ██ ██    ██ ██ ██  ██ █████   ██ "
PrintGreen "██   ██ ██    ██ ██  ██ ██ ██         "
PrintGreen "██████   ██████  ██   ████ ███████ ██ "
PrintGreen ""
