#
# Configuration strictly related to Sway process.
#

# reload the configuration file
bindcode $mod+Shift+$keycode_r reload

# exit sway (logs you out of your Wayland session)
bindcode $mod+Shift+$keycode_e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'
