# Mouse related configuration.
#
# Read `man 5 sway-input` for more information about this section.

# Disable changing focus with cursor move
focus_follows_mouse no


# Touchpad
# - inverted scrolling
# - 1 finger is left, 2 fingers are right, 3 fingers are middle button
# - click to tap
input type:touchpad {
    natural_scroll disabled
    tap_button_map lrm
    tap enabled
}

# Mouse
# - disabled acceleration (for better egonomics)
input type:pointer {
    accel_profile flat
    pointer_accel 0
}
