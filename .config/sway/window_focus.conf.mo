#
# Moving focus around.
#

bindcode $mod+$keycode_h focus left
bindcode $mod+$keycode_j focus down
bindcode $mod+$keycode_k focus up
bindcode $mod+$keycode_l focus right
