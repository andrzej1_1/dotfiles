#
# Gaps modes.
#

set $mode_gaps Gaps: (o)uter, (i)nner, (h)orizontal, (v)ertical, (t)op, (r)ight, (b)ottom, (l)eft
set $mode_gaps_outer Outer Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_inner Inner Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_horiz Horizontal Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_verti Vertical Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_top Top Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_right Right Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_bottom Bottom Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_left Left Gaps: +|-|0 (local), Shift + +|-|0 (global)
bindcode $mod+Shift+$keycode_g mode "$mode_gaps"

mode "$mode_gaps" {
	bindcode $keycode_o      mode "$mode_gaps_outer"
	bindcode $keycode_i      mode "$mode_gaps_inner"
	bindcode $keycode_h      mode "$mode_gaps_horiz"
	bindcode $keycode_v      mode "$mode_gaps_verti"
	bindcode $keycode_t      mode "$mode_gaps_top"
	bindcode $keycode_r      mode "$mode_gaps_right"
	bindcode $keycode_b      mode "$mode_gaps_bottom"
	bindcode $keycode_l      mode "$mode_gaps_left"
	bindcode $keycode_Return mode "$mode_gaps"
	bindcode $keycode_Escape mode "default"
}

mode "$mode_gaps_outer" {
	bindcode $keycode_plus  gaps outer current plus 5
	bindcode $keycode_minus gaps outer current minus 5
	bindcode $keycode_0     gaps outer current set 0

	bindcode Shift+$keycode_plus  gaps outer all plus 5
	bindcode Shift+$keycode_minus gaps outer all minus 5
	bindcode Shift+$keycode_0     gaps outer all set 0

	bindcode $keycode_Return mode "$mode_gaps"
	bindcode $keycode_Escape mode "default"
}
mode "$mode_gaps_inner" {
	bindcode $keycode_plus  gaps inner current plus 5
	bindcode $keycode_minus gaps inner current minus 5
	bindcode $keycode_0     gaps inner current set 0

	bindcode Shift+$keycode_plus  gaps inner all plus 5
	bindcode Shift+$keycode_minus gaps inner all minus 5
	bindcode Shift+$keycode_0     gaps inner all set 0

	bindcode $keycode_Return mode "$mode_gaps"
	bindcode $keycode_Escape mode "default"
}
mode "$mode_gaps_horiz" {
	bindcode $keycode_plus  gaps horizontal current plus 5
	bindcode $keycode_minus gaps horizontal current minus 5
	bindcode $keycode_0     gaps horizontal current set 0

	bindcode Shift+$keycode_plus  gaps horizontal all plus 5
	bindcode Shift+$keycode_minus gaps horizontal all minus 5
	bindcode Shift+$keycode_0     gaps horizontal all set 0

	bindcode $keycode_Return mode "$mode_gaps"
	bindcode $keycode_Escape mode "default"
}
mode "$mode_gaps_verti" {
	bindcode $keycode_plus  gaps vertical current plus 5
	bindcode $keycode_minus gaps vertical current minus 5
	bindcode $keycode_0     gaps vertical current set 0

	bindcode Shift+$keycode_plus  gaps vertical all plus 5
	bindcode Shift+$keycode_minus gaps vertical all minus 5
	bindcode Shift+$keycode_0    gaps vertical all set 0

	bindcode $keycode_Return mode "$mode_gaps"
	bindcode $keycode_Escape mode "default"
}
mode "$mode_gaps_top" {
	bindcode $keycode_plus  gaps top current plus 5
	bindcode $keycode_minus gaps top current minus 5
	bindcode $keycode_0     gaps top current set 0

	bindcode Shift+$keycode_plus  gaps top all plus 5
	bindcode Shift+$keycode_minus gaps top all minus 5
	bindcode Shift+$keycode_0     gaps top all set 0

	bindcode $keycode_Return mode "$mode_gaps"
	bindcode $keycode_Escape mode "default"
}
mode "$mode_gaps_right" {
	bindcode $keycode_plus  gaps right current plus 5
	bindcode $keycode_minus gaps right current minus 5
	bindcode $keycode_0     gaps right current set 0

	bindcode Shift+$keycode_plus  gaps right all plus 5
	bindcode Shift+$keycode_minus gaps right all minus 5
	bindcode Shift+$keycode_0     gaps right all set 0

	bindcode $keycode_Return mode "$mode_gaps"
	bindcode $keycode_Escape mode "default"
}
mode "$mode_gaps_bottom" {
	bindcode $keycode_plus  gaps bottom current plus 5
	bindcode $keycode_minus gaps bottom current minus 5
	bindcode $keycode_0     gaps bottom current set 0

	bindcode Shift+$keycode_plus  gaps bottom all plus 5
	bindcode Shift+$keycode_minus gaps bottom all minus 5
	bindcode Shift+$keycode_0     gaps bottom all set 0

	bindcode $keycode_Return mode "$mode_gaps"
	bindcode $keycode_Escape mode "default"
}
mode "$mode_gaps_left" {
	bindcode $keycode_plus  gaps left current plus 5
	bindcode $keycode_minus gaps left current minus 5
	bindcode $keycode_0     gaps left current set 0

	bindcode Shift+$keycode_plus  gaps left all plus 5
	bindcode Shift+$keycode_minus gaps left all minus 5
	bindcode Shift+$keycode_0     gaps left all set 0

	bindcode $keycode_Return mode "$mode_gaps"
	bindcode $keycode_Escape mode "default"
}

#
# Default settings.
#
gaps inner 15
gaps outer 0

#
# Border styles.
#

# class                 border  backgr. text    indicator child_border
client.focused          #4c7899 #285577 #ffffff #2e9ef4   #00ff00

default_border pixel 2
