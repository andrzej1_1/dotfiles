#
# Volume bindings.
# They are allowed to work when screen is locked.
#

bindcode --locked Shift+$keycode_Next exec pactl set-sink-volume @DEFAULT_SINK@ +5%
bindcode --locked Shift+$keycode_Prior exec pactl set-sink-volume @DEFAULT_SINK@ -5%
bindcode --locked Shift+$keycode_Home exec pactl set-sink-mute @DEFAULT_SINK@ toggle
bindcode --locked Shift+$keycode_End exec pactl set-source-mute @DEFAULT_SOURCE@ toggle
