#
# Workspaces.
#

# switch to workspace
bindcode $mod+$keycode_1 workspace 1
bindcode $mod+$keycode_2 workspace 2
bindcode $mod+$keycode_3 workspace 3
bindcode $mod+$keycode_4 workspace 4
bindcode $mod+$keycode_5 workspace 5
bindcode $mod+$keycode_6 workspace 6
bindcode $mod+$keycode_7 workspace 7
bindcode $mod+$keycode_8 workspace 8
bindcode $mod+$keycode_9 workspace 9
bindcode $mod+$keycode_0 workspace 10

# move focused container to workspace
bindcode $mod+Shift+$keycode_1 move container to workspace 1
bindcode $mod+Shift+$keycode_2 move container to workspace 2
bindcode $mod+Shift+$keycode_3 move container to workspace 3
bindcode $mod+Shift+$keycode_4 move container to workspace 4
bindcode $mod+Shift+$keycode_5 move container to workspace 5
bindcode $mod+Shift+$keycode_6 move container to workspace 6
bindcode $mod+Shift+$keycode_7 move container to workspace 7
bindcode $mod+Shift+$keycode_8 move container to workspace 8
bindcode $mod+Shift+$keycode_9 move container to workspace 9
bindcode $mod+Shift+$keycode_0 move container to workspace 10
