#
# Autostart.
#

# systemd integration (https://github.com/swaywm/sway/wiki/Systemd-integration)
exec "systemctl --user import-environment; systemctl --user start sway-session.target"

# wlsunset (redshift replacement)
exec wlsunset -l 55.7:12.6 -t 5600:3600 -g 0.8

# notifications
exec mako

# run all apps for sway environment from XDG autostart
exec dex -a -e sway
