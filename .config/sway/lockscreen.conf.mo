#
# Lockscreen.
#

# Super+L: Blurred wallpaper
bindcode $sup+$keycode_l exec swaylock -i ~/Pictures/Wallpapers/space1_blurred.jpg -K

# Super+Backspace: Transparent lockscreen
bindcode $sup+$keycode_BackSpace exec swaylock -c 00000000 -K
