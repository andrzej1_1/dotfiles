#
# Layout stuff.
#

# You can "split" the current object of your focus with
# $mod+v or $mod+b, for horizontal and vertical splits
# respectively.
bindcode $mod+$keycode_v splith
bindcode $mod+$keycode_b splitv

# Switch the current container between different layout styles
bindcode $mod+$keycode_s layout stacking
bindcode $mod+$keycode_w layout tabbed
bindcode $mod+$keycode_e layout toggle split

# Make the current focus fullscreen
bindcode $mod+$keycode_f fullscreen

# Toggle the current focus between tiling and floating mode
bindcode $mod+Shift+$keycode_Space floating toggle

# Swap focus between the tiling area and the floating area
bindcode $mod+$keycode_Space focus mode_toggle

# move focus to the parent container
bindcode $mod+$keycode_a focus parent

# kill focused window
bindcode $mod+Shift+$keycode_q kill

# kill clicked window
bindsym --whole-window $mod+button2 kill
