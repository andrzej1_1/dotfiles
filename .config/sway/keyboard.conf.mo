#
# Keyboard settings.
#

input * {
    # Layout:
    # - PL, Dvorak programmer (dvp)
    # NOTE: For traditional PL qwerty, use 'legacy' variant.
    xkb_layout pl
    xkb_variant dvp
    #xkb_options grp:alt_shift_toggle
}
