#
# Resizing mode.
#

mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindcode $keycode_h resize shrink width 10px
    bindcode $keycode_j resize grow height 10px
    bindcode $keycode_k resize shrink height 10px
    bindcode $keycode_l resize grow width 10px

    # return to default mode
    bindcode $keycode_Return mode "default"
    bindcode $keycode_Escape mode "default"
}

bindcode $mod+$keycode_r mode "resize"
