#
# Screenshots.
#

# partial screenshot with output selection
set $partial_screenshot Partial screenshot output: clipboard (c), file (f)

mode "$partial_screenshot" {
	bindcode $keycode_c exec grim -g "$(slurp)" - | wl-copy, mode "default"
	bindcode $keycode_f exec grim -g "$(slurp)" $(xdg-user-dir PICTURES)/$(date +'%Y-%m-%d-%H%M%S_grim.png'), mode "default"

	bindcode $keycode_Return mode "$partial_screenshot"
	bindcode $keycode_Escape mode "default"
}

bindcode $mod+$keycode_Print mode "$partial_screenshot"

# full area
bindcode $keycode_Print exec grim -c $(xdg-user-dir PICTURES)/$(date +'%Y-%m-%d-%H%M%S_grim.png')
