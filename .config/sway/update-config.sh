#!/usr/bin/env bash

# Determines the script location
pushd . > /dev/null
SELF_DIR="${BASH_SOURCE[0]}";
if ([ -h "${SELF_DIR}" ]) then
      while([ -h "${SELF_DIR}" ]) do cd `dirname "$SELF_DIR"`; SELF_DIR=`readlink "${SELF_DIR}"`; done
fi
cd `dirname ${SELF_DIR}` > /dev/null
SELF_DIR=`pwd`;
popd  > /dev/null


#
# Floating dropdown config
#

# settings
BAR_HEIGHT=30
WIDTH_PERCENT=70
HEIGHT_PERCENT=40
CENTER_POS_X_PERCENT=50
CENTER_POS_Y_PERCENT=65

# getting current resolution
mode_line=$(swaymsg -t get_outputs -p | grep -i "Current mode")
monitor_width=$(echo $mode_line | sed 's/^.* \([0-9]*\)x\([0-9]*\) .*/\1/')
monitor_height=$(echo $mode_line | sed 's/^.* \([0-9]*\)x\([0-9]*\) .*/\2/')

# calulcations
width=$(( WIDTH_PERCENT * monitor_width / 100 ))
height=$(( HEIGHT_PERCENT * (monitor_height - BAR_HEIGHT) / 100 ))
lefttop_x=$(( (CENTER_POS_X_PERCENT * monitor_width / 100) - (width / 2) ))
lefttop_y=$(( (CENTER_POS_Y_PERCENT * (monitor_height - BAR_HEIGHT) / 100) - (height / 2) ))

echo "Dropdown position and size:"
echo "X: $lefttop_x Y: $lefttop_y (width=$width, height=$height)"

WIDTH=$width HEIGHT=$height LEFTTOP_X=$lefttop_x LEFTTOP_Y=$lefttop_y mo $SELF_DIR/floating_terminal.conf.mo > $SELF_DIR/floating_terminal.conf

#
# Other templates (TODO make all templates generated with one-line)
#

mo $SELF_DIR/autostart.conf.mo > $SELF_DIR/autostart.conf
mo $SELF_DIR/bar.conf.mo > $SELF_DIR/bar.conf
mo $SELF_DIR/file_browser.conf.mo > $SELF_DIR/file_browser.conf
mo $SELF_DIR/gaps.conf.mo > $SELF_DIR/gaps.conf
mo $SELF_DIR/keyboard.conf.mo > $SELF_DIR/keyboard.conf
mo $SELF_DIR/launcher.conf.mo > $SELF_DIR/launcher.conf
mo $SELF_DIR/layout.conf.mo > $SELF_DIR/layout.conf
mo $SELF_DIR/lockscreen.conf.mo > $SELF_DIR/lockscreen.conf
mo $SELF_DIR/mouse_n_touchpad.conf.mo > $SELF_DIR/mouse_n_touchpad.conf
mo $SELF_DIR/output.conf.mo > $SELF_DIR/output.conf
mo $SELF_DIR/resize.conf.mo > $SELF_DIR/resize.conf
mo $SELF_DIR/screenshot.conf.mo > $SELF_DIR/screenshot.conf
mo $SELF_DIR/sound.conf.mo > $SELF_DIR/sound.conf
mo $SELF_DIR/sway_process.conf.mo > $SELF_DIR/sway_process.conf
mo $SELF_DIR/terminal.conf.mo > $SELF_DIR/terminal.conf
mo $SELF_DIR/window_focus.conf.mo > $SELF_DIR/window_focus.conf
mo $SELF_DIR/window_move.conf.mo > $SELF_DIR/window_move.conf
mo $SELF_DIR/workspaces.conf.mo > $SELF_DIR/workspaces.conf

echo "Done!"
