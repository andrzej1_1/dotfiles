#
# Background.
#

output * bg ~/Pictures/Wallpapers/space1.jpg fill

#
# Light control (laptop only).
#

bindsym XF86MonBrightnessUp exec light -A 10
bindsym XF86MonBrightnessDown exec light -U 10
