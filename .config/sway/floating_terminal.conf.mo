#
# Floating terminal.
#

bindcode $keycode_grave scratchpad show
for_window [title="^Floating terminal - System$"] floating enable, resize set {{WIDTH}} {{HEIGHT}}, move position {{LEFTTOP_X}} {{LEFTTOP_Y}}, move scratchpad
